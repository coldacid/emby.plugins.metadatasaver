# Emby.Plugins.MetadataSaver

Adds a task to Emby that forces metadata to be saved to XML.

[![Build status](https://ci.appveyor.com/api/projects/status/rm8xiu9xwecfgw9k/branch/master?svg=true)](https://ci.appveyor.com/project/coldacid/emby-plugins-metadatasaver/branch/master)

-----

This repository contains a plugin that adds a new scheduled task for
creating/updating local metadata files for all items in one's media library.

## Building

This project can be built with the `dotnet` CLI but also provides a solution
file for use with Visual Studio 2017. To build with the CLI:

```sh
> dotnet build
```

You can optionally specify a `DeployToEmby` property to automatically copy the
plugin to your Emby plugins folder (assuming that you're on Windows and Emby is
installed at `%APPDATA%\MediaBrowser-Server`):

```sh
> dotnet build -p DeployToEmby=1
```
