﻿namespace MediaBrowser.Plugins.MetadataSaver
{
    /// <summary>
    /// Provides configuration options for the Save Library Metadata plugin.
    /// </summary>
    public class PluginConfiguration : MediaBrowser.Model.Plugins.BasePluginConfiguration
    {
    }
}
