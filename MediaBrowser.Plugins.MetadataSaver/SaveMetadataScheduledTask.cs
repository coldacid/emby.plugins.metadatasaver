﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediaBrowser.Controller.Entities;
using MediaBrowser.Controller.Library;
using MediaBrowser.Controller.Providers;
using MediaBrowser.Model.Logging;
using MediaBrowser.Model.Tasks;

namespace MediaBrowser.Plugins.MetadataSaver
{
    /// <summary>
    /// Provides a scheduled task for saving library metadata to file.
    /// </summary>
    public class SaveMetadataScheduledTask : IScheduledTask
    {
        private readonly ILogManager _logManager;
        private readonly ILibraryManager _libraryManager;
        private readonly IProviderManager _providerManager;

        /// <summary>
        /// Creates a new instance of the <see cref="SaveMetadataScheduledTask"/> class.
        /// </summary>
        /// <param name="logManager">The log manager instance for Emby.</param>
        /// <param name="libraryManager">The Emby library manager.</param>
        /// <param name="providerManager">The Emby provider manager.</param>
        public SaveMetadataScheduledTask(ILogManager logManager, ILibraryManager libraryManager, IProviderManager providerManager)
        {
            _logManager = logManager ?? throw new ArgumentNullException(nameof(logManager));
            _libraryManager = libraryManager ?? throw new ArgumentNullException(nameof(libraryManager));
            _providerManager = providerManager ?? throw new ArgumentNullException(nameof(providerManager));
        }

        /// <inheritdoc />
        public string Name => "Save library metadata";

        /// <inheritdoc />
        public string Key => "Charabaruk.MediaLibrary.MetadataSaver.SaveMetadata";

        /// <inheritdoc />
        public string Category => "Library";

        /// <inheritdoc />
        public string Description => "Creates or updates metadata XML files for all library items.";

        /// <inheritdoc />
        public IEnumerable<TaskTriggerInfo> GetDefaultTriggers()
        {
            return Enumerable.Empty<TaskTriggerInfo>();
        }

        /// <inheritdoc />
        public Task Execute(CancellationToken cancellationToken, IProgress<double> progress)
        {
            var logger = _logManager.GetLogger("MetadataSaver.SaveMetadata");
            return Task.Run(
                () => new Operation(_libraryManager.RootFolder, logger, cancellationToken, progress, _providerManager).Execute(),
                cancellationToken);
        }

        private class Operation
        {
            private readonly ILogger _logger;
            private readonly CancellationToken _cancellationToken;
            private readonly IProgress<double> _progress;

            private readonly IProviderManager _providerManager;

            private readonly BaseItem _root;
            private readonly long _totalCount;

            private readonly Stack<BaseItem> _items;

            private long _completed = 0;
 
            public Operation(BaseItem root, ILogger logger, CancellationToken cancellationToken, IProgress<double> progress,
                IProviderManager providerManager)
            {
                _logger = logger ?? throw new ArgumentNullException(nameof(logger));
                _progress = progress ?? throw new ArgumentNullException(nameof(progress));

                _cancellationToken = cancellationToken;

                _items = new Stack<BaseItem>();

                _providerManager = providerManager ?? throw new ArgumentNullException(nameof(providerManager));

                _root = root ?? throw new ArgumentNullException(nameof(root));

                var rootFolder = root as Folder;
                _totalCount = 1 + rootFolder?.RecursiveChildren.LongCount() ?? 1;
            }

            public void Execute()
            {
                StackExecute(_root);

                while (_items.Count > 0 && !_cancellationToken.IsCancellationRequested)
                {
                    StackExecute(_items.Pop());
                }
            }

            private void StackExecute(BaseItem item)
            {
                var name = item.Path ?? item.Name;
                var mediaType = string.IsNullOrWhiteSpace(item.MediaType) ? item.GetType().Name : item.MediaType;

                if (item.SupportsLocalMetadata)
                {
                    _logger.Debug("{1} '{0}' supports local metadata; triggering save", name, mediaType);
                    try
                    {
                        _providerManager.SaveMetadata(item, (ItemUpdateType)0x1F);
                    }
                    catch (Exception ex)
                    {
                        _logger.ErrorException("Could not save metadata for {0}", ex, name);
                    }
                }
                else
                {
                    _logger.Debug("{1} '{0}' does not support local metadata; cannot save", name, mediaType);
                }

                _completed++;
                _progress.Report((double)_completed / _totalCount);

                if (!item.IsFolder) return;
                if (!(item is Folder folder))
                {
                    _logger.Warn("{0} claims to be Folder but isn't", name);
                    return;
                }

                foreach (var child in folder.Children)
                    _items.Push(child);
            }
        }
    }
}
