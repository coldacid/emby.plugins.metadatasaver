﻿using System;

using MediaBrowser.Common.Configuration;
using MediaBrowser.Model.Serialization;

namespace MediaBrowser.Plugins.MetadataSaver
{
    /// <summary>
    /// Declares the Save Library Metadata plugin.
    /// </summary>
    public class Plugin : MediaBrowser.Common.Plugins.BasePlugin<PluginConfiguration>
    {
        /// <inheritdoc />
        public Plugin(IApplicationPaths applicationPaths, IXmlSerializer xmlSerializer)
            : base(applicationPaths, xmlSerializer)
        { }

        /// <inheritdoc />
        public override string Name => "Media Library Metadata Saver";

        /// <inheritdoc />
        public override Guid Id => new Guid("b5a5ee19-5df9-43e6-ac4d-6245b3b8ee6a");
    }
}
